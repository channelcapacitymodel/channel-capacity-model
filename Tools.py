import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt

class Tools:
    def add(self, a, b):
        for i in np.arange(0, len(a), 1):
            a[i] = a[i] + b[i]
        return a

    def multiple(self, a, b):
        result = np.zeros(len(a))
        for i in np.arange(0, len(a), 1):
            result[i] = a[i] * b[i]
        return result

    def getValueByKey(self, map, key):
        if key in map:
            return map[key]
        return 0

    def saveCarsInHourOnRoad(self, DayTime, summ_number_of_cars_on_road):
        plt.figure()
        plt.plot(DayTime, summ_number_of_cars_on_road, label='Number cars in hour')
        plt.xlabel('Hours')
        plt.ylabel('Number of cars')
        plt.legend(loc='best')
        plt.savefig('Average number of cars per hour on road')

    def saveAvSpeedPerUser(self,DayTime, avThroughtputForUser, operatorNum):
        plt.plot(DayTime, avThroughtputForUser, label='Average Speed per user for operator'+str(operatorNum))
        plt.xlabel('Hour')
        plt.ylabel('Mbit/s')
        plt.axhline(y=0, color='black')
        plt.axvline(x=0, color='black')
        plt.show()
        #plt.savefig('Av speed per user in hour')

    def saveAvUsersInSectorPerHOur(self,DayTime, numberOfUsersInSectorAtTheSameTime):
        plt.plot(DayTime, numberOfUsersInSectorAtTheSameTime)
        plt.xlabel('Hour')
        plt.ylabel('Number of users')
        plt.axhline(y=0, color='black')
        plt.axvline(x=0, color='black')
        plt.show()
        # plt.savefig('Av number of users per hour')