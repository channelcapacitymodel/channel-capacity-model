import math as math
import numpy as np
from fbm import FBM
import Tools as t
import scipy.stats as st
import Tools as tools


class Distribution:
    def getNumberOfCars(self, n):  # poisson distribution
        return np.round(np.random.poisson(n, 1)[0])

    def getVelocityOfCar(self, v):  # normal distribution
        mu, sigma = v, 15
        return np.random.normal(mu, sigma, 1)[0]

    def getCumulativeTrafficFromOneCar(self, length, T, n, hurst):
        passengers_in_car = self.passengersInCar()
        f = FBM(n, hurst, length, method='daviesharte')
        fbm_sample = f.fbm()  # Generate a fBm realization
        t_values_for_fbm = f.times()  # Get the times associated with the fBm
        # T = 10  # scale factor
        M = (1. / 2) * (T * passengers_in_car)
        sigma = t_values_for_fbm[-1] ** (hurst)
        traffic = M * t_values_for_fbm + (T ** hurst) * math.sqrt(passengers_in_car) * sigma * fbm_sample
        for n, b in enumerate(traffic):  # replace all negative values on 0
            if b < 0:
                traffic[n] = 0
        return traffic

    #def getCumulativeTrafficFromOneUser(self, length, rate, traffic_figure_is_needed):
    #    trafficPerSec = self.calculateTrafficPerSec(rate, length)
    #    if traffic_figure_is_needed == True:
    #        tools.Tools().saveTrafficFromOneUser(
    #            trafficPerSec)  # save volume of traffic from one user during passing the sector
    #    traffic = 0
    #    for value in trafficPerSec.itervalues():
    #        traffic = traffic + value
    #    return traffic  # summ trafic for passing time of the sector

    #def calculateTrafficPerSec(self, rate, length):
    #    #packet_size = 1500.  # ethernet packet size in byte
    #    burst_time = 1.  # s on period
    #    idle_time = 3.  # s off period
    #    shape = 1.2  # pareto param

    #   passing_time = 0
    #    res = {}  # map: sec(0, 1,2,3) - number of bits
    #    tool = t.Tools()
    #    #interval = (packet_size * 8) / (rate * 10 ** 6)  # transform to bits
    #    #burstlen = burst_time / interval
    #    # b1 = burstlen * (shape - 1) / shape
    #    b1 = burst_time * (shape - 1) / shape
    #    b2 = idle_time * (shape - 1) / shape
    #    while 1:
    #        pareto = st.pareto(shape, loc=0, scale=b1)
    #        next_burstlen = pareto.rvs(size=1)[0]  # on period
    #        if passing_time + next_burstlen > length:
    #            break
    #        passing_time = passing_time + next_burstlen
    #        volume = rate * next_burstlen
    #        res[math.floor(passing_time)] = volume + tool.getValueByKey(res,
    #                                                                    math.floor(
    #                                                                        passing_time))  # number of bits in On period

    #       pareto = st.pareto(shape, loc=0, scale=b2)
    #        next_idle_time = pareto.rvs(size=1)[0]  # off period
    #        if passing_time + next_idle_time > length:
    #            break
    #        passing_time = passing_time + next_idle_time

    #       if passing_time > length:
    #            break
    #    return res

    def passengersInCar(self):
        average_passengers_in_car = 2
        passengers_in_car = np.random.poisson(average_passengers_in_car, 1)[0]
        if passengers_in_car == 0:  # 0 passengers in car - is impossible
            passengers_in_car = 1
        if passengers_in_car > 5:  # more than 5 passengers is impossible
            passengers_in_car == 5
        return passengers_in_car
