import math as math
import numpy as np
import transportModel as tm
import scipy.constants as const
import matplotlib.pyplot as plt


class Sector:
    def __init__(self, lineNum, bs, ms):  # lineNum - number of line on the road
        self.bs = bs
        self.ms = ms
        self.lineNum = lineNum
        self.v = self.maxSpeedByModulation()
        self.averageSpeed = self.calculateAverageSpeed()

    def maxSpeedByModulation(self):  # poka tolko FDD podderzhka
        v = [(x * 1000. * self.bs.mimo) / 10 ** 6 for x in self.bs.channel.tbs]  # in Mb/s
        return v

    def calculateAverageSpeed(self):
        return np.mean(self.v)  # average speed in mean

    def calculateSizeOfSector(self, mode):
        modes = {'DL': self._maxPathLossDL(),
                 'UL': self._maxPathLossUL()
                 }

        fast_fed = 7.5
        slow_fed = 7.5
        Ms = math.sqrt(fast_fed**2+ slow_fed**2)
        lMax = modes[mode]
        lMaxWithReserv = lMax - Ms
        if mode == 'DL':
            C = 0.8 + (1.1 * math.log10(self.bs.channel.frequinsy) - 0.7) * self.ms.antennaH - 1.56 * math.log10(
                self.bs.channel.frequinsy)

            # a = (lMax - 69.55 - 26.16 * math.log10(self.bs.channel.frequinsy) + 13.82 * math.log10(self.bs.antennaH) + C) / \
            #    (44.9 - 6.55 * math.log10(self.bs.antennaH)) #city

            #a = (lMax + 2 * (math.log10(self.bs.channel.frequinsy / 28)) ** 2 - 64.15 - 26.16 * math.log10(
            #    self.bs.channel.frequinsy) + 13.82 * math.log10(self.bs.antennaH) + C) / (
            #    44.9 - 6.55 * math.log10(self.bs.antennaH))  # sub urban

            a = (lMaxWithReserv + 4.78 * (math.log10(self.bs.channel.frequinsy)) ** 2 - 44.49 * math.log10(
                self.bs.channel.frequinsy) - 19.61 + 13.82 * math.log10(self.bs.antennaH) + C) / (
                    44.9 - 6.55 * math.log10(self.bs.antennaH)) #rural urban
        if mode == 'UL':
            C = 0.8 + (1.1 * math.log10(self.bs.channel.frequinsy) - 0.7) * self.ms.antennaH - 1.56 * math.log10(
                self.bs.channel.frequinsy)

            # a = (lMax - 69.55 - 26.16 * math.log10(self.bs.channel.frequinsy) + 13.82 * math.log10(self.ms.antennaH) + C) / \
            #    (44.9 - 6.55 * math.log10(self.ms.antennaH)) #city

            #a = (lMax + 2 * (math.log10(self.bs.channel.frequinsy / 28)) ** 2 - 64.15 - 26.16 * math.log10(
            #    self.bs.channel.frequinsy) + 13.82 * math.log10(self.bs.antennaH) + C) / (
            #    44.9 - 6.55 * math.log10(self.bs.antennaH))  # sub urban

            a = (lMaxWithReserv + 4.78 * (math.log10(self.bs.channel.frequinsy)) ** 2 - 44.49 * math.log10(
                self.bs.channel.frequinsy) - 19.61 + 13.82 * math.log10(self.bs.antennaH) + C) / (
                    44.9 - 6.55 * math.log10(self.bs.antennaH)) #rural urban

        self.size = 10 ** (a)
        return self.size

    def _maxPathLossDL(self):
        eirp = self.bs.power + self.bs.gain - 1
        e = 6
        T = 290
        f = 10 * math.log10((const.k * T * self.bs.channel.bandwith * 10 ** 6 / (18 ** -3)))  # kTB, B-bandwith in hz
        g = e + f
        h = -7
        i = g + h
        j = 3
        k = 0.4
        m = 0
        carLossless = 8
        mibolityLossless = 3
        lMax = eirp - i - j - k + self.ms.gain - m
        return lMax

    def _maxPathLossUL(self):
        eirp = self.ms.power + self.ms.gain
        e = 2
        T = 290
        f = 10 * math.log10((const.k * T * self.bs.channel.bandwith * 10 ** 6 / (18 ** -3)))  # kTB, B-bandwith in hz
        g = e + f
        h = -7
        i = g + h
        j = 2
        k = 2
        m = 2
        carLossless = 8
        mibolityLossless = 3
        lMax = eirp - i - j - k + self.bs.gain - m
        return lMax

    def calculateSpeedByDistance(self):
        self.v = list(reversed(self.v))
        self._dist = (1 - self.v / np.amax(self.v)) * self.size
        #print(self.v)
        #print(self._dist)
        m, c = np.polyfit(self._dist, self.v, 1)
        newx = np.array([0, self.size])
        newy = m * newx + c
        plt.plot(newx, newy)
        plt.step(self._dist, self.v)
        plt.xticks(np.arange(0, max(self._dist) + 1, 1.0))
        plt.grid(True)
        plt.xlabel('Distance, km')
        plt.ylabel('Access Speed, Mbit/s')
        #plt.legend(loc='best')
        plt.savefig('Speed in sector')
        return self._dist
