import matplotlib.pyplot as plt
import numpy as np


class Smo:
    def __init__(self, throughtput, recomendedAvDelay, hurst):
        self._hurst = hurst
        self._throughtput = throughtput * 10 ** 6
        self._recomendedAvDelay = recomendedAvDelay
        self._avServiceTime = self._calculateServiceTime()
        self.avWaitTime = self._calculateAvWaitTime()
        self.load = self.calculateLoad()


    def _calculateAvWaitTime(self):
        return self._recomendedAvDelay - self._avServiceTime

    def _calculateServiceTime(self):
        length_of_packet = 1500. * 8
        return length_of_packet / self._throughtput

    def calculateLoad(self):
        #return 2* self.avWaitTime / (2* self.avWaitTime+self._avServiceTime)  # load for specicific average delay
        return 1 - ((self._avServiceTime/(2*self.avWaitTime)) ** (self._hurst/(1-self._hurst)))

    def savePlotAvDelayFromLoad(self):
        x = np.linspace(0.,0.99,100)
        y = self._avServiceTime * (x**(1./(2*(1-self._hurst)))) / (2*((1-x)**(self._hurst/(1-self._hurst))))
        plt.figure()
        plt.plot(x, y)
        plt.xlabel('Load')
        plt.ylabel('Average waiting time')
        #plt.show()
        plt.savefig('Dependency average waiting time from load of BS')
