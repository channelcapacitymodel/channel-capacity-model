import Tools as t
import numpy as np
class TransportModel:
    def __init__(self, v0, ro_max, n):
        self.v0 = v0
        self.ro_max = ro_max
        self.n = n

    def getDensity(self, v):
        self.v = v
        self.ro = self.ro_max * ((self.v0 - v) / self.v0) ** (2. / (self.n + 1))
        return self.ro

    def getVelocity(self, ro):
        self.ro = ro
        self.v = self.v0 * (1 - (self.ro / self.ro_max) ** ((self.n + 1) / 2))
        return self.v

    def numberOfCarsInSectorPerHour(self, density, velocity, sectorSize):
        tool=t.Tools()
        numberCarsPerKm=tool.multiple(velocity, density)  # number of cars in all sector
        numberCarsPerSector=np.round(numberCarsPerKm * sectorSize)
        return numberCarsPerSector

    def numberOfCarsInSectorByDensity(self, density, sectorSize):
        a=np.round(density * sectorSize)
        for n, b in enumerate(a):  # replace all 0 values on 1
            if b == 0:
                a[n] = 1
        return a
