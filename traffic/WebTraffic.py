import numpy as np
import matplotlib.pyplot as plt
import distributions as distr
import Tools as tools


# 110-average speed on sector
# 380 - N - maximum number of cars in 1 line, line-namber of lines N*line=380
# Transport = tm.TransportModel(110., 380., 0.)  # second param - number of car on 1 km (N*numberof lines)
# average_velocity = np.array(
#    [105., 100., 100., 100., 95., 95., 95., 95., 75., 80., 90., 95., 95., 95., 95., 95., 90., 85., 85., 75., 90., 100.,
#     100., 100.])
# average_velocity_hour = np.array(
#    [85., 85., 80., 80., 80., 80., 80., 80., 80., 75., 75., 75., 75., 75., 80., 80., 80., 80., 75., 80., 80., 85.,
#    85., 85.])
# average_velocity_hour = np.array(
#    [80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80.,
#    80., 80.])

class WebTraffic:
    # def __init__(self, T, n, hurst):
    #    self._T = T
    #    self._n = n
    #    self._hurst=hurst

    def __init__(self, T, n, hurst):
        self._T = T
        self._n = n
        self._hurst = hurst

    def webTrafficPerTime(self, average_velocity, sectorLength, avCarsNumInHour):
        DayTime = np.arange(0, len(average_velocity), 1)
        tool = tools.Tools()
        traffic_hour = []
        approx_traffic=[]
        iterations = 1
        S=avCarsNumInHour
        distribution = distr.Distribution()
        for i in np.arange(0, iterations, 1):  # cycle to Iterations - number of approximation
            for hour in DayTime:
                print('hour ', hour)
                poisson_cars = distribution.getNumberOfCars(n=S[hour])  # get random value of cars from poisson distr
                traffic_cars = 0
                print('cars ', poisson_cars)
                for car in np.arange(0, poisson_cars, 1):  # cycle for each car in hour
                    v = distribution.getVelocityOfCar(average_velocity[hour])  # get speed of car by maxwell distribution
                    length = sectorLength / v * 3600
                    traffic_car = distribution.getCumulativeTrafficFromOneCar(length, self._T,
                                                                                  self._n, self._hurst);  # cumulative traffic from one user

                    traffic_cars = traffic_car + traffic_cars
                traffic_hour.append(np.amax(traffic_cars))
            if i==0:
                approx_traffic=np.zeros(len(traffic_hour))
            approx_traffic=tool.add(approx_traffic, traffic_hour)
        result = approx_traffic / iterations  # usrednyaem znacheniya posle N iteraziy
        return result



    def webTrafficPerTimeOnOff(self, average_velocity, sectorLength, avCarsNumInHour, numberOfUsersInSectorAtTheSameTime, numberOfPassengers):
        lanThroughtput=[100.0 / x  for x in numberOfUsersInSectorAtTheSameTime]#speed of the lan interface to BS mb/s
        tool = tools.Tools()
        distribution = distr.Distribution()
        DayTime = np.arange(0, len(average_velocity), 1)
        traffic_hour=[]
        approx_traffic=[]
        iterations = 3
        S=avCarsNumInHour
        cars_num=[]
        approx_cars_num=[]
        traffic_figure_is_needed=True
        for i in np.arange(0, iterations, 1):  # cycle to Iterations - number of approximation
            for hour in DayTime:
                print('hour', hour)
                poisson_cars = distribution.getNumberOfCars(n=S[hour])  # get random value of cars from poisson distr
                traffic_cars = 0
                print('Number of cars=', poisson_cars)
                cars_num.append(poisson_cars)
                for car in np.arange(0, poisson_cars,1):  # cycle for each car in hour
                    v = distribution.getVelocityOfCar(average_velocity[hour])
                    length = sectorLength / v * 3600 #in seconds
                    traffic_car = 0
                    #for user in np.arange(0, distribution.passengersInCar(), 1): #cycle for each user in car
                    for user in np.arange(0, numberOfPassengers, 1):  # cycle for each user in car
                        traffic_user=distribution.getCumulativeTrafficFromOneUser(length, lanThroughtput[hour], traffic_figure_is_needed)
                        traffic_figure_is_needed=False
                        traffic_car=traffic_user+traffic_car
                    traffic_cars=traffic_car+traffic_cars
                traffic_hour.append(traffic_cars)
            if i==0:
                approx_traffic=np.zeros(len(traffic_hour))
                approx_cars_num=np.zeros(len(cars_num))
            approx_traffic=tool.add(approx_traffic, traffic_hour)
            approx_cars_num=tool.add(approx_cars_num, cars_num)
        tool.saveCarsInHourOnRoad(DayTime, approx_cars_num / iterations) #save plot with number of cars data
        result = approx_traffic / iterations  # usrednyaem znacheniya posle N iteraziy
        return result
