from entity import BaseStation as bs
from entity import MobileStation as ms
from entity import RadioChannel as rc
from entity import Sector as s
from smo import Smo as queue
from entity import Operator as o
from traffic import WebTraffic as webTraffic

import transportModel as tm
import numpy as np
import matplotlib.pyplot as plt
import distributions as distr
import Tools as tools

tool = tools.Tools()
operatorsEquipment = {}
i = 1
while 1:
    print(str(i) + ' operator:')
    print("Frequinsy")
    frequinsy = input()
    print("Bandwith:")
    bandwith = input()
    channel = rc.RadioChannel(frequinsy=frequinsy, bandwith=bandwith)
    print("BS Antenna High:")
    power = input()
    print("Base Station Power:")
    antennaH = input()
    print("BS Antenna gain:")
    gain = input()
    print("BS number of mimo:")
    mimo = input()
    baseStation = bs.BaseStation(power=power, antennaH=antennaH, gain=gain, channel=channel, mimo=mimo)
    operatorsEquipment[i] = baseStation
    print('Do you want input data for next operator?')
    nextOperator = raw_input()
    if nextOperator == 'yes' or nextOperator == 'y':
        i = i + 1
    else:
        break

ms = ms.UserEquipment(power=5., antennaH=1, gain=3, channel=channel, mimo=1)

Transport = tm.TransportModel(130., 380., 0.)  # second param - number of car on 1 km (N*numberof lines)
average_velocity_hour_one = np.array(
     [105., 100., 100., 100., 95., 95., 95., 95., 75., 80., 90., 95., 95., 95., 95., 95., 90., 85., 85., 75., 90., 100.,
     100., 100.])

average_velocity_hour_two = np.array(
    [110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110., 110.,
     110., 110.])

#average_velocity_hour_one = np.array( #right line
#    [105., 90., 105, 105, 105, 105])

#average_velocity_hour_two = np.array( #left line
#    [105., 90., 105, 105, 105, 105])

hurst=0.75
DayTime = np.arange(0, len(average_velocity_hour_one), 1)
TDensity_one = Transport.getDensity(average_velocity_hour_one)
TDensity_two = Transport.getDensity(average_velocity_hour_two)
distributions = distr.Distribution()
traffic = webTraffic.WebTraffic(T=1, n=1000, hurst=0.75)
operators = {}
numberOfUsersInCar=2
generated_traffic=[]
for key, value in operatorsEquipment.iteritems():
    sector = s.Sector(lineNum=2., bs=value, ms=ms)
    throughtput = sector.averageSpeed
    print(throughtput)
    sectorLength = sector.calculateSizeOfSector(mode='DL')
    avNumCarsInSector_one = Transport.numberOfCarsInSectorByDensity(TDensity_one, sectorLength)
    avNumCarsInSector_two = Transport.numberOfCarsInSectorByDensity(TDensity_two, sectorLength)
    avNumCarsInSector=tool.add(avNumCarsInSector_one, avNumCarsInSector_two)

    speedByDistance = sector.calculateSpeedByDistance()

    numberOfUsersInSectorAtTheSameTime = []
    for carInHour in avNumCarsInSector / len(operatorsEquipment): #for specific operator
        numberOfPassengers=0
        for car in np.arange(0, carInHour, 1):
            numberOfPassengers = numberOfUsersInCar+numberOfPassengers
        numberOfUsersInSectorAtTheSameTime.append(numberOfPassengers)

    generated_traffic.append(traffic.webTrafficPerTime(average_velocity_hour_one, sectorLength, numberOfUsersInSectorAtTheSameTime))
    operators[key]=o.Operator(sectorLength, throughtput, numberOfUsersInSectorAtTheSameTime, avNumCarsInSector / len(operatorsEquipment), key)

avDelay=0.01


if len(operators)>1:
    plt.figure()
    for key in operators.iterkeys():
        plt.plot(DayTime, operators[key].users_num_per_hour, label='Number of users in sector for operator' + str(operators[key].operator_num))
    plt.xlabel('Hour')
    plt.ylabel('Number of users per operator')
    plt.legend(loc='best')
    plt.axhline(y=0, color='black')
    plt.axvline(x=0, color='black')
    plt.savefig('Number of users in sector for operator')

#for key in operators.iterkeys():
#    print(operators[key].users_num_per_hour)
#    if key==1:
#        summNumberOfUsersPerHour=operators[key].users_num_per_hour[:]
#    else:
#        for hour in np.arange(0, len(operators[key].users_num_per_hour), 1):
#            summNumberOfUsersPerHour[hour]=summNumberOfUsersPerHour[hour]+operators[key].users_num_per_hour[hour]

#if len(operators)>1:
#    plt.figure()
#    for key in operators.iterkeys():
#        plt.plot(DayTime, operators[key].users_num_per_hour, label='Number of users in sector for operator' + str(operators[key].operator_num))
#    plt.xlabel('Hour')
#    plt.ylabel('Number of users per operator')
#    plt.legend(loc='best')
#    plt.axhline(y=0, color='black')
#    plt.axvline(x=0, color='black')
#    plt.savefig('Number of users in sector for operator')


#plt.figure()
#for key in operators.iterkeys():
#    plt.plot(DayTime, summNumberOfUsersPerHour)
#plt.xlabel('Hour')
#plt.ylabel('Summ number of users')
#plt.axhline(y=0, color='black')
#plt.axvline(x=0, color='black')
#plt.savefig('Summ number of users in all sector')

#plt.figure()
#for key in operators.iterkeys():
#    smo = queue.Smo(operators[key].througthput, avDelay, hurst)
#    smo.savePlotAvDelayFromLoad()
#    print(smo.load)
#    avThroughtputForUser = smo.load * operators[key].througthput / operators[key].users_num_per_hour
#    plt.plot(DayTime, avThroughtputForUser, label='Average Speed per user for operator' + str(operators[key].operator_num))
#plt.xlabel('Hour')
#plt.ylabel('Mbit/s')
#plt.legend(loc='best')
#plt.axhline(y=0, color='black')
#plt.axvline(x=0, color='black')
#plt.savefig('Average speed per user for operator')






