from entity import BaseStation as bs
from entity import MobileStation as ms
from entity import RadioChannel as rc
from entity import Sector as s

channel = rc.RadioChannel(frequinsy=3500., bandwith=3.)
bs = bs.BaseStation(power=24., antennaH=8., gain=16., channel=channel, mimo=2.)
ms = ms.UserEquipment(power=5., antennaH=1, gain=3, channel=channel, mimo=1)
sector = s.Sector(car_vel=105., lineNum=2., bs=bs, ms=ms, maxNumberOfCarsPerkm=180.,
                 maxSpeedInSector=110.)

a=sector.maxSpeedByModulation()
print('Speed by modulation', a)
b=sector.averageSpeed()
print('Avearge max speed', b)
c=sector.averageSpeedForUser()
print('Avearge for user speed', c)

print('Number of cars in sector', sector.carsNumPerKm)

d = sector.calculateSizeOfSector(mode='DL')
print('Size DL=',d)
d = sector.calculateSizeOfSector(mode='UL')
print('Size UL=',d)


