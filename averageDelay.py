import numpy as np
import matplotlib.pyplot as plt
import transportModel as tm
import distributions as distr
import Tools as tools

TVelocity = np.array(
    [105., 100., 100., 100., 95., 95., 95., 95., 75., 80., 90., 95., 95., 95., 95., 95., 90., 85., 85., 75., 90., 100.,
     100., 100.])
# TVelocity = np.array(
#    [85., 85., 80., 80., 80., 80., 80., 80., 80., 75., 75., 75., 75., 75., 80., 80., 80., 80., 75., 80., 80., 85.,
#    85., 85.])
# TVelocity = np.array(
#    [80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80., 80.,
#     80., 80.])

max_speed = 110.
lines = 2
max_car_per_line = 190
tool = tools.Tools()
transport = tm.TransportModel(max_speed, lines * max_car_per_line,
                              0.)  # second param - number of car on 1 km (N*numberof lines)
sectorLength = 0.5  # km
max_delay = 0.1  # w 100 ms for VOIP
# w = np.arange(0, max_delay, 0.01)
w = max_delay
average_num_car = sectorLength * transport.getDensity(TVelocity)
poisson_num_car = []
distribution = distr.Distribution()
for num in average_num_car:
    poisson_num_car.append(distribution.getNumberOfCars(n=num))

alpha = 1  # average bits/packets from one car/user

lam = [x * alpha for x in poisson_num_car]


def getRelativeSpeedOfChannel(lam,
                              w,
                              n):  # Regarding the average load from the user, lam-incoming stream per our, w-averafe delay
    D = (n * lam * w) ** 2 + 4 * lam * w * n
    m1 = (n * lam * w + D ** (1. / 2)) / (n * 2 * w)
    m2 = (n * lam * w - D ** (1. / 2)) / (n * 2 * w)
    if m1 > m2 and m1 > 0:
        return m1
    elif m2 > m1 and m2 > 0:
        return m2
    else:
        return 0


def calculateDelayForCurrentChannel(lam, m, n):
    w = lam / (m * n * (m - lam))
    return w


iteration = 30
average_m_mm1 = []
average_m_md1 = []

delay_mm1 = []  # calculate delay during the day for traffic with calculated for specific m for each hour a day
delay2mm1 = []
delay_md1 = []
delay2md1 = []
m_mm1 = []
m_md1 = []

for i in np.arange(0, len(poisson_num_car), 1):
    n = 1
    m_mm1.append(getRelativeSpeedOfChannel(lam[i], w, 1))
    print(lam[i], m_mm1[i])
    n = 2
    m_md1.append(getRelativeSpeedOfChannel(lam[i], w, 2))

for i in np.arange(0, len(poisson_num_car), 1):
    a = poisson_num_car[i] * alpha / m_mm1[i]
    print(a)

po_mm1 = poisson_num_car * alpha / np.amax(m_mm1)
po_md1 = poisson_num_car * alpha / np.amax(m_md1)

for i in np.arange(0, len(poisson_num_car), 1):
    n = 1
    delay_mm1.append(
        calculateDelayForCurrentChannel(lam[i], np.amax(m_mm1), n))  # second param - const - sr velichina m

for i in np.arange(0, len(poisson_num_car), 1):
    n = 2
    delay_md1.append(
        calculateDelayForCurrentChannel(lam[i], np.amax(m_md1), n))  # second param - const - sr velichina m

hours = np.arange(0, len(TVelocity), 1)
fig = plt.figure(figsize=(6, 4))
sub1 = fig.add_subplot(221)
sub1.plot(hours, m_mm1, 'r--', hours, m_md1, 'b:')  # plot m(t hour per day)
sub2 = fig.add_subplot(222)
sub2.plot(hours, po_mm1, 'r--', hours, po_md1, 'b:')
sub3 = fig.add_subplot(223)
sub3.plot(hours, delay_mm1, 'r--', hours, delay_md1, 'b:')  # plot m(t hour per day)
plt.show()
