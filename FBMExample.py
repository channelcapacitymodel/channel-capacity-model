from fbm import FBM
import matplotlib.pyplot as plt

hurst = 0.8
n = 1000
length = 1000
f = FBM(n, hurst, length, method='daviesharte')
fbm_sample = f.fbm()  # Generate a fBm realization
t_values_for_fbm = f.times()  # Get the times associated with the fBm

plt.figure()
plt.plot(t_values_for_fbm, fbm_sample, 'r--')
plt.axhline(y=0, color='black')
plt.axvline(x=0, color='black')
plt.savefig('FBM Sample')