from entity import BaseStation as bs
from entity import MobileStation as ms
from entity import RadioChannel as rc
from entity import Sector as s

import numpy as np
import matplotlib.pyplot as plt
import Tools as tools

tool = tools.Tools()
operatorsEquipment = {}
i = 1
while 1:
    print(str(i) + ' operator:')
    print("Frequinsy")
    frequinsy = input()
    print("Bandwith:")
    bandwith = input()
    channel = rc.RadioChannel(frequinsy=frequinsy, bandwith=bandwith)
    print("BS Antenna High:")
    power = input()
    print("Base Station Power:")
    antennaH = input()
    print("BS Antenna gain:")
    gain = input()
    print("BS number of mimo:")
    mimo = input()
    baseStation = bs.BaseStation(power=power, antennaH=antennaH, gain=gain, channel=channel, mimo=mimo)
    operatorsEquipment[i] = baseStation
    print('Do you want input data for next operator?')
    nextOperator = raw_input()
    if nextOperator == 'yes' or nextOperator == 'y':
        i = i + 1
    else:
        break

ms = ms.UserEquipment(power=5., antennaH=1, gain=3, channel=channel, mimo=1)
hurst=0.75
avDelay=0.01
sizeOfPacket = 1500. * 8
maxLoad=[]
througthput = {}
for key, value in operatorsEquipment.iteritems():
    sector = s.Sector(lineNum=2., bs=value, ms=ms)
    avThr = sector.averageSpeed
    print(avThr)
    througthput[operatorsEquipment[key].channel.bandwith]=avThr
print(througthput)

plt.figure()
laod = np.linspace(0., 0.99, 100)
for bandwidth, v in  througthput.iteritems():
    v = v * 10**6
    serviceTime =  sizeOfPacket / v
    waitTime = sizeOfPacket * (laod ** (1. / (2 * (1 - hurst)))) / (2 * ((1 - laod) ** (hurst / (1 - hurst))))
    print(bandwidth, 1 - ((serviceTime / (2 * avDelay)) ** (hurst / (1 - hurst))))
    maxLoad.append(1 - ((serviceTime / (2 * avDelay)) ** (hurst / (1 - hurst))))
    plt.plot(laod, waitTime)
plt.xlabel('Avearge Load')
plt.ylabel('Average waiting time')
plt.savefig('Dependency average waiting time from load of BS')
