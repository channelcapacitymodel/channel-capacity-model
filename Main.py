from entity import BaseStation as bs
from entity import MobileStation as ms
from entity import RadioChannel as rc
from entity import Sector as s
from smo import Smo as queue
from entity import Operator as o

import numpy as np
import matplotlib.pyplot as plt
import Tools as tools

tool = tools.Tools()
operatorsEquipment = {}

print("Use Ran Sharing?")
isRanSharingMode = raw_input()
if isRanSharingMode == 'yes' or isRanSharingMode == 'y':
    print("Enter number of operators, which use base station")
    operatorsNum = input()
    baseStationsNum = 1
else:
    print("Enter number of base stations")
    operatorsNum = 1
    baseStationsNum = input()

for i in np.arange(1, baseStationsNum + 1, 1):
    print('Base statio ' + str(i) + ':')
    print("Frequinsy")
    frequinsy = input()
    print("Bandwith:")
    bandwith = input()
    channel = rc.RadioChannel(frequinsy=frequinsy, bandwith=bandwith, operators_num=float(operatorsNum))
    print("BS Antenna High:")
    antennaH = input()
    print("Base Station Power:")
    power = input()
    print("BS Antenna gain:")
    gain = input()
    print("BS number of mimo:")
    mimo = input()
    baseStation = bs.BaseStation(power=power, antennaH=antennaH, gain=gain, channel=channel, mimo=mimo)
    operatorsEquipment[i] = baseStation

ms = ms.UserEquipment(power=3., antennaH=1.5, gain=1.5, channel=channel, mimo=1)
avNumberOfPassengersInCar = 2
avDelay = 0.01
hurst = 0.75
coef_oversubscr = 4.
operators = {}
transport_intensive = [266, 251, 232, 220, 208., 224., 235., 278., 306., 362., 430., 506., 546., 562., 615., 675., 688.,
                       640., 669., 610., 552., 468., 395., 325.]
transport_velocity = [120., 120., 120., 120., 120., 120., 120., 110., 110., 110., 110., 100., 105., 110., 100., 100.,
                      100., 105., 100., 105., 115., 120., 120., 120.]
DayTime = np.arange(0, len(transport_velocity), 1)
transport_density = []
for i in range(len(transport_velocity)):
    transport_density.append(transport_intensive[i] / transport_velocity[i])
#print(transport_density)

for key, value in operatorsEquipment.iteritems():
    sector = s.Sector(lineNum=2., bs=value, ms=ms)
    numberOfUsersForOperator = []
    avNumberOfCarsInsSectorInTheSameTime = []
    avUsersNumInSector = []
    ulSize = sector.calculateSizeOfSector(mode='UL')
    print "UL Sector Size {}".format(ulSize)
    dlSize = sector.calculateSizeOfSector(mode='DL')
    print "DL Sector Size {}".format(dlSize)
    avNumberOfCarsInsSectorInTheSameTime = [dlSize * d for d in transport_density]
    avUsersNumInSector = [cars_num * (avNumberOfPassengersInCar / coef_oversubscr) for cars_num in
                          avNumberOfCarsInsSectorInTheSameTime]
    #print(avUsersNumInSector)
    throughtput = sector.averageSpeed
    print "Average speed={}".format(throughtput)
    speedByDistance = sector.calculateSpeedByDistance()
    if isRanSharingMode == 'yes' or isRanSharingMode == 'y':
        numberOfUsersForOperator = [users / 4. for users in avUsersNumInSector]
    else:
        numberOfUsersForOperator = [users / 4. for users in avUsersNumInSector]
    operators[key] = o.Operator(dlSize, throughtput, numberOfUsersForOperator, key)

plt.figure()
plt.plot(DayTime, transport_intensive, 'bo')
plt.plot(DayTime, transport_intensive, 'b')
plt.xlabel("Hour")
plt.grid(True)
plt.xticks(np.arange(0, max(DayTime)+1 , 1.0))
plt.yticks(np.arange(0, max(transport_intensive)+100 , 100.0))
plt.ylabel("Number of passing cars")
plt.savefig('Number of passing cars in day')

# plot users intensive
plt.figure()
for key in operators.iterkeys():
    plt.plot(DayTime, operators[key].users_num_per_hour, 'bo')
    plt.plot(DayTime, operators[key].users_num_per_hour, 'b')
plt.axhline(y=0, color='black')
plt.axvline(x=0, color='black')
plt.xticks(np.arange(0, max(DayTime)+1 , 1.0))
plt.grid(True)
plt.xlabel("Hour")
plt.ylabel("Number of  users in sector in the same time")
plt.savefig('Number of  users in sector in the same time')


# plot av speed for user - result
plt.figure()
for key in operators.iterkeys():
    avThroughtputForUser = []
    smo = queue.Smo(operators[key].througthput, avDelay, hurst)
    print "Load = {}".format(smo.load)
    for user in operators[key].users_num_per_hour:
        if user < 1:
            user = 1
        avThroughtputForUser.append(operators[key].througthput / (user * smo.load))
    print "Min speed={}".format(min(avThroughtputForUser))
    plt.plot(DayTime, avThroughtputForUser, 'bo')
    plt.plot(DayTime, avThroughtputForUser, 'b')
plt.xlabel('Hour')
plt.ylabel('Mbit/s')
plt.grid(True)
plt.xticks(np.arange(0, max(DayTime)+1 , 1.0))
plt.axhline(y=0, color='black')
plt.axvline(x=0, color='black')
plt.savefig('Average speed per user for operator')
